import image_extract
import PIL.Image as P_Image
import argparse
import pathlib
import time


progress_time = 0


def on_progress(progress, last_progress):
    global progress_time

    current_time = time.monotonic()
    if 2 <= (current_time - progress_time) or progress == last_progress:
        _ = "{:.1%}".format(progress / last_progress)
        print(progress, "/", last_progress, "=", _)

        progress_time = current_time


parser = argparse.ArgumentParser()

parser.add_argument("path")
_ = "--lum"
parser.add_argument(_, default=None, help="Luminance threshold [0, 1]. Default: 0.67")
_ = "--area"
parser.add_argument(_, default=None, help="Area threshold [0, 1]. Default: 0.001")
parser.add_argument(
    "--mask",
    default=False,
    action="store_true",
    help="Create mask instead, for parameter tuning.",
)

arguments = parser.parse_args()

if arguments.lum is not None:
    arguments.lum = float(arguments.lum)

if arguments.area is not None:
    arguments.area = float(arguments.area)


path = pathlib.Path(arguments.path)

image = P_Image.open(path)
bool_array = image_extract.get_bool_array(image, luminance_threshold=arguments.lum)

if arguments.mask:
    _ = pathlib.Path(path.parent, "{}.mask{}".format(path.stem, path.suffix))
    image_extract.get_masked(bool_array, image).save(_)

else:
    _ = image_extract.get_geometries(
        bool_array, area_threshold=arguments.area, on_progress=on_progress
    )
    for index, geometry in enumerate(_):
        _ = pathlib.Path(path.parent, "{}.{}{}".format(path.stem, index, path.suffix))
        image_extract.extract_image(geometry, image).save(_)
