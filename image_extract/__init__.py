import numba
import numpy
import PIL.Image as P_Image
import shapely.geometry as s_geometry
import random


_REORDER_INDICES = {  # vector orientations: coordinate indices
    (-2, -1): [2, 3, 0, 1],
    (-2, 1): [1, 0, 3, 2],
    (-1, -2): [2, 1, 0, 3],
    (-1, 0): [1, 2, 3, 0],
    (-1, 2): [2, 1, 0, 3],
    (0, -1): [3, 2, 1, 0],
    (0, 1): [0, 1, 2, 3],
    (1, -2): [3, 0, 1, 2],
    (1, 0): [0, 3, 2, 1],
    (1, 2): [3, 0, 1, 2],
    (2, -1): [2, 3, 0, 1],
    (2, 1): [1, 0, 3, 2],
}


def get_bool_array(image, luminance_threshold=None):
    if luminance_threshold is None:
        luminance_threshold = 0.67

    return numpy.asarray(image.convert("L")) <= (luminance_threshold * 255)


def get_geometries(bool_array, area_threshold=None, on_progress=None):
    if area_threshold is None:
        area_threshold = 0.001

    height, width = bool_array.shape

    pool_array, pool_count = _flood(bool_array)

    # unset inside
    center_array = pool_array[1:-1, 1:-1]
    mask_array = numpy.ones_like(center_array, dtype=bool)

    for row_index in range(3):
        for column_index in range(3):
            _ = width - 2 + column_index
            mask_array &= (
                pool_array[row_index : height - 2 + row_index, column_index:_,]
                == center_array
            )

    center_array[mask_array] = 0
    #

    rectangle_set = set()
    coordinate_lists = {pool_id: [] for pool_id in range(1, pool_count + 1)}
    progress = 0
    last_progress = len(pool_array) + len(coordinate_lists)

    for row_index, row in enumerate(pool_array):
        for column_index, pool_id in enumerate(row):
            if pool_id == 0:
                continue

            coordinate_lists[pool_id].append((row_index + 0.5, column_index + 0.5))

        if on_progress is not None:
            progress += 1
            on_progress(progress, last_progress)

    for index, (pool_id, coordinate_list) in enumerate(coordinate_lists.items()):
        _ = (
            s_geometry.Point(coordinate_list[0])
            if len(coordinate_list) == 1
            else (
                s_geometry.LineString
                if len(coordinate_list) == 2
                else s_geometry.Polygon
            )(coordinate_list)
        ).convex_hull.buffer(0.5)
        rectangle = _get_rectangle(_)

        while True:
            for set_rectangle in rectangle_set:
                if set_rectangle.geometry.intersects(rectangle.geometry):
                    break

            else:
                break

            rectangle_set.remove(set_rectangle)
            _ = set_rectangle.convex_hull.union(rectangle.convex_hull).convex_hull
            rectangle = _get_rectangle(_)

        rectangle_set.add(rectangle)

        if on_progress is not None:
            progress += 1
            on_progress(progress, last_progress)

    min_area = width * height * area_threshold

    for rectangle in rectangle_set:
        geometry = rectangle.geometry

        if _get_area(geometry) < min_area:
            continue

        yield geometry


@numba.njit
def _flood(array):
    row_number, column_number = array.shape

    pool_array = numpy.zeros_like(array, dtype=numpy.uint16)
    pool_id = 1

    for row_index in range(row_number):
        for column_index in range(column_number):
            _ = array[row_index, column_index]
            if not (_ and pool_array[row_index, column_index] == 0):
                continue

            indices = [(row_index, column_index)]

            while len(indices) != 0:
                row_index, column_index = indices.pop()

                pool_array[row_index, column_index] = pool_id

                for next_row_index in range(row_index - 1, row_index + 2):
                    if not (0 <= next_row_index and next_row_index < row_number):
                        continue

                    for next_column_index in range(column_index - 1, column_index + 2):
                        _ = 0 <= next_column_index and next_column_index < column_number
                        if not _:
                            continue

                        _ = array[next_row_index, next_column_index]
                        if _ and pool_array[next_row_index, next_column_index] == 0:
                            indices.append((next_row_index, next_column_index))

            pool_id += 1

    return (pool_array, pool_id - 1)


def _get_rectangle(convex_hull):
    return _Rectangle(convex_hull.minimum_rotated_rectangle, convex_hull)


class _Rectangle:
    def __init__(self, geometry, convex_hull):
        super().__init__()

        self.geometry = geometry
        self.convex_hull = convex_hull


def _get_area(geometry):
    if isinstance(geometry, s_geometry.Polygon):
        return geometry.area

    if isinstance(geometry, s_geometry.Point):
        return 1

    return geometry.length


def extract_image(geometry, image, _roll=0, _reverse=False):
    coordinate_array = _get_rectangle_coordinates(geometry)

    if _roll != 0:
        coordinate_array = numpy.roll(coordinate_array, _roll, axis=0)

    if _reverse:
        coordinate_array = coordinate_array[::-1]

    # adjust coordinates
    # - first: top left
    # - order: clockwise
    # - could be done more efficiently
    vector = coordinate_array[1] - coordinate_array[0]
    current_angle = numpy.arctan2(vector[0], vector[1])
    target_angle = round(current_angle / (numpy.pi / 2)) * (numpy.pi / 2)

    sin = numpy.sin(-(target_angle - current_angle))
    cos = numpy.cos(-(target_angle - current_angle))
    _ = numpy.array([[cos, -sin], [sin, cos]]) @ coordinate_array.T
    rotated_coordinates = _.T

    vectors = rotated_coordinates[1:3] - rotated_coordinates[:2]

    height, width = numpy.abs(vectors).max(axis=0)

    _ = numpy.round(numpy.arctan2(vectors[:, 0], vectors[:, 1]) / (numpy.pi / 2))
    coordinate_array = coordinate_array[_REORDER_INDICES[tuple(_)]]
    #

    return image.transform(
        (round(width), round(height)),
        P_Image.Transform.QUAD,
        data=coordinate_array[[0, 3, 2, 1], ::-1].flatten(),
        resample=P_Image.Resampling.BICUBIC,
        fillcolor="white",
    )


def _get_rectangle_coordinates(geometry):
    coordinate_array = numpy.asarray(_get_coordinates(geometry))

    if len(coordinate_array) == 1:
        coordinate_array = numpy.repeat(coordinate_array, 4, axis=0)

    elif len(coordinate_array) == 2:
        _ = [coordinate_array, coordinate_array[::-1, :]]
        coordinate_array = numpy.vstack(_)

    else:
        coordinate_array = coordinate_array[:4]

    return coordinate_array


def _get_coordinates(geometry):
    if isinstance(geometry, s_geometry.Polygon):
        geometry = geometry.exterior

    return geometry.coords


def get_masked(bool_array, image):
    _ = P_Image.composite(
        P_Image.new("RGB", image.size, color=(0, 255, 0)),
        P_Image.new("RGB", image.size, color=(255, 0, 0)),
        P_Image.fromarray(bool_array),
    )
    return P_Image.blend(_, image.convert("RGB"), 0.5)
