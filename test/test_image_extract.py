import image_extract
import numpy
import PIL.Image as P_Image
import PIL.ImageDraw as P_ImageDraw


def test_extract_image():
    image = P_Image.new("L", (100, 100), color="white")
    draw = P_ImageDraw.Draw(image)

    lines = [[(2, 0), (0, 0), (0, 2), (2, 2)], [(1, 1), (3, 1), (3, 3), (1, 3)]]
    _ = max(x for line in lines for x, _ in line)
    draw_size = numpy.array([_, max(y for line in lines for _, y in line)])
    image_size = numpy.array([image.width, image.height])
    for line in lines:
        _ = image_size / 2 + ((numpy.array(line) / draw_size) - 0.5) * image_size / 2
        draw.line([(float(x), float(y)) for x, y in _], width=3, fill="black")

    for angle in range(0, 360, 15):
        # avoid angles where float errors may decide the outcome
        if (angle + 45) % 90 == 0:
            continue

        _ = P_Image.Resampling
        rotated_image = image.rotate(angle, fillcolor="white", resample=_.BICUBIC)
        images = _get_images(rotated_image, 0, False)

        for roll in range(4):
            for reverse in [False, True]:
                assert _get_images(rotated_image, roll, reverse) == images


def _get_images(image, roll, reverse):
    _ = image_extract.get_geometries(image_extract.get_bool_array(image))
    return [
        image_extract.extract_image(geometry, image, _roll=roll, _reverse=reverse)
        for geometry in _
    ]
