# Image extract

is a Python package which isolates and extracts images from an image. The motivating use case: scan multiple things at once where you don't care about slight crop and blur due to automatic isolation. It uses [*Pillow*](https://github.com/python-pillow/Pillow) for image read, transform and write, [*Shapely*](https://github.com/shapely/shapely) for geometric operations and [*Numba*](https://github.com/numba/numba) for jit, among others.

## Quickstart

1. Install [*Pipenv*](https://github.com/pypa/pipenv), e.g. `python -m pip install pipenv`
2. Create virtual environment and install dependencies: `python -m pipenv run pip install numba numpy Pillow shapely`
4. Run as script: `python -m pipenv run python -m image_extract --help`

## Documentation

See [`image_extract/__main__.py`](./image_extract/__main__.py) and main steps in [Details](#details).

## Motivation

I had a considerable number of items I wanted to scan but didn't want to align, scan and crop each item individually. The solution: loosely arrange as many items per scan and isolate them in post. Obviously this problem is already solved, but I couldn't find a solution that was scriptable and wasn't part of a much larger application.

The first version took some time to implement but was fun, as opposed to performing the same task over and over. By now, the effort is easily amortized and the results were reliably satisfying. So it was time to refactor, improve, test, document and publish.

## Details

The steps performed are:

1. Convert image to bool array
    * `image_extract.get_bool_array` converts an image to grayscale and uses a fixed luminance threshold
2. `image_extract.get_geometries`
    1. Identify connected regions using flood filling
    2. Convert region exteriors to *Shapely* geometries
    3. Merge geometries whose minimum rotated rectangles intersect
    4. Yield minimum rotated rectangles
3. Extract sub image
    * `image_extract.extract_image` performs Quad transform